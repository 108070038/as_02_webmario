const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {

    @property({type:cc.AudioClip})
    select: cc.AudioClip = null;


    private txtEmail: string = "";

    private txtPassword: string = "";

    private username: string = "";

    onload(){
        //this.playBGM();
    }

    /*
    playBGM(){
        cc.audioEngine.playMusic(this.bgm, true);
    }
    */

    signinBtn(){

        this.playSound(this.select);

        var par = this.node.getChildByName("text_bg");
        this.txtEmail = par.getChildByName("email_txt").getComponent(cc.EditBox).string;
        this.txtPassword = par.getChildByName("password_txt").getComponent(cc.EditBox).string;
        this.username = par.getChildByName("username_txt").getComponent(cc.EditBox).string;
        var tEmail = this.txtEmail;
        var tusername = this.username;
        var userRef = firebase.database().ref('users/' + tEmail.replace('.', ''));
        
        // email, password, life, state, highscore

        firebase.auth().createUserWithEmailAndPassword(this.txtEmail, this.txtPassword).then(function(result) {
            
            userRef.set({
                username: tusername,
                email: tEmail,
                life: '3',
                state: '1', // 1: small, 2: big
                highscore: '0'
            });//profile_picture : imageUrl

            alert("Success" + result);
            cc.director.loadScene("menu");
        }).catch(function(error) {
            alert("Error!!!\n" + error);
        });
        
    }

    playSound(clip: cc.AudioClip){
        cc.audioEngine.playEffect(clip, false);
    }
    
    loadMenuScene(){
        this.playSound(this.select);
        cc.director.loadScene("menu");
    }

    
}
