import Player from "./player";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameMgr extends cc.Component 
{
    
    @property(cc.Node)
    background: cc.Node = null;

    @property(Player)
    player: Player = null;

    @property(cc.Node)
    pauseIcon: cc.Node = null;

    @property(cc.Node)
    scoreNode: cc.Node = null;

    @property(cc.Node)
    highestScoreNode: cc.Node = null;

    @property(cc.Node)
    camera: cc.Node = null;

    @property(cc.Node)
    timer: cc.Node = null;

    @property(cc.Node)
    life: cc.Node = null;

    @property(cc.Node)
    world: cc.Node = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    stageClearSound: cc.AudioClip = null;

    debugCollider: cc.Node = null;

    gathering: boolean = false;

    energyValue: number = 0;

    private backgroundSize = 256;

    private wallSize = 384;

    private physicManager: cc.PhysicsManager = null;

    private score: number = 0;

    private highestScore: number = 0;

    private pause: boolean = false;

    private playerX: number = 0;

    private playerY: number = 0;

    onLoad()
    {
        this.physicManager = cc.director.getPhysicsManager();
        this.physicManager.enabled = true;
        this.physicManager.gravity = cc.v2 (0, -3000);
        cc.audioEngine.playMusic(this.bgm, true);
        
        var origin_life;
        var origin_state;

        var user = firebase.auth().currentUser;
        var tEmail = user.email;
        var userRef = firebase.database().ref('users/' + tEmail.replace('.', ''));
        userRef.once('value').then((snapshot) => {
            if (snapshot.exists()) {
              //user_name = snapshot.val().username;
              origin_life = snapshot.val().life;
              origin_state = snapshot.val().state;
              //origin_high = snapshot.val().highscore;
            }
            else {
              alert("No User!!");
            }
        }).then(()=>{
            this.life.getComponent(cc.Label).string = origin_life;
            if(origin_state == '2'){
                this.player.getComponent("player").isBig = true;
            }
        });
    }

    start () {
        
        this.playerX = this.player.node.x;
        this.playerY = this.player.node.y;
        
        let s = this.world.getComponent(cc.Label).string;
        Global.now_stage = Number(s);

        this.schedule(()=>{
            let time = this.timer.getComponent(cc.Label).string;
            if(time == '1'){
                cc.director.pause();
                if(!this.player.getComponent("player").isDie) {
                    this.player.getComponent("player").playerDie();
                    this.player.getComponent("player").isDie = true;
                }
                this.unscheduleAllCallbacks();
            }
            let newTime: number;
            newTime = Number(time);
            newTime -= 1;
            this.timer.getComponent(cc.Label).string = String(newTime);
            cc.log(time);
        }, 1);
        
    }

    update(dt)
    {

    }

    record(die: boolean, big: boolean){
        
        var user_name;
        var origin_life;
        var origin_state;
        var origin_high;

        var new_life = '3';
        var new_state = '1';
        var new_score;

        var user = firebase.auth().currentUser;
        var tEmail = user.email;

        var userRef = firebase.database().ref('users/' + tEmail.replace('.', ''));
        userRef.once('value').then((snapshot) => {
            if (snapshot.exists()) {
              user_name = snapshot.val().username;
              origin_life = snapshot.val().life;
              origin_state = snapshot.val().state;
              origin_high = snapshot.val().highscore;
            }
            else {
              alert("No User!!");
            }
        }).then(() =>{

            cc.log(origin_life);
            cc.log(origin_state);
            if(die){
                if(origin_life == '1') new_life = '3';
                else new_life = String(Number(origin_life) - 1);
            }
            else {
                new_life = origin_life;
                if(big) new_state = '2';
                else new_state = '1';
            }
    
            var postData;
    
            if(Number(origin_high) < this.score){
                new_score = String(this.score);
            }
            else new_score = origin_high;
    
            postData = {
                highscore: new_score,
                life: new_life,
                state: new_state
            };
            
            userRef.update(postData);
    
        }).then(() =>{
            
            if(!die || die && (origin_life == '1')) {
                if(!die){
                    cc.audioEngine.stopAll();
                    cc.audioEngine.playEffect(this.stageClearSound, false);
                }
                cc.director.pause();
                setTimeout(() => { 
                    cc.director.loadScene("howtoplay");
                    cc.director.resume();
                }, 4000);
            }
            else {
                cc.director.pause();
                setTimeout(() => {
                    if(Global.now_stage == 1){
                        cc.director.loadScene("game1");
                    }
                    else cc.director.loadScene("game2");
                    cc.director.resume();
                }, 4000);
            }
        });

        
        
        
    }

    playerScoreAdd(points: number){
        cc.log("score++");
        if(points == 500){

        }
        else if(points == 100){

        }
        this.score += points;
        this.scoreNode.getComponent(cc.Label).string = String(this.score);

    }

    randomChoosePlatform()
    {
        let rand = Math.random();

        //0: normal, 1: conveyor
        let prob = [8, 1];
        let sum = prob.reduce((a,b)=>a+b);
        for(let i = 1; i < prob.length; i++)
            prob[i] += prob[i-1];
        for(let i = 0; i < prob.length; i++)
        {
            prob[i] /= sum;
            if(rand <= prob[i])
                return i;
        }
    }



    updateHighestScore(score: number)
    {
        this.highestScore = score;
        this.highestScoreNode.getComponent(cc.Label).string = (Array(4).join("0") + this.highestScore.toString()).slice(-4);
    }

    updateScore(score: number)
    {
        this.score = score;
        this.scoreNode.getComponent(cc.Label).string = (Array(4).join("0") + this.score.toString()).slice(-4);
    }

    
    gamePause()
    {
        if(this.pause)
            this.pause = false;
        else
            this.pause = true;
        if(this.pause)
        {
            this.pauseIcon.active = true;
            this.scheduleOnce(()=>{
                cc.game.pause();
            }, 0.1);
        }
        else
        {
            this.pauseIcon.active = false;
            cc.game.resume();
        }
    }

    gameOver()
    {
        /*
        this.startIcon.active = true;
        this.player.node.active = false;
        this.inGame = false;
        this.unschedule(this.gatherEnergy);
        this.updateEnergyBar(0);
        this.gathering = false;

        cc.audioEngine.stopMusic();
        */
    }

    gameEnd()
    {
        cc.game.end();
    }
}
