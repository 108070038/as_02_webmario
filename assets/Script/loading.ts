//import {howtoplay} from "./howtoplay";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Loading extends cc.Component {
    
    //@property({type:cc.AudioClip})
    //bgm: cc.AudioClip = null;

    
    //private audioID: number = 0;
    private times: number = 0;
    
    private anim = null;

    onload(){
        this.anim = this.getComponent(cc.Animation);

    }

    private method loading() {
        if(this.times == 10){
            //this.anim.stop("mario_HI");
            this.loadGameScene();
        }
        else this.times += 1;
        
        //this.anim.play("mario_HI");
    }

    
    loadGameScene(){
        if(Global.next_stage == 1) cc.director.loadScene("game1");
        else if(Global.next_stage == 2) cc.director.loadScene("game2");
    }

    

}
