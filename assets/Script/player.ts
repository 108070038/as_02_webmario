const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component 
{
    @property(cc.SpriteFrame)
    marioDie: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    goombaDie: cc.SpriteFrame = null;

    @property({type:cc.AudioClip})
    powerup: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    powerdown: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    reserve: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    dieSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    jumpSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    stageClearSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    goombaDieSound: cc.AudioClip = null;

    @property({type:cc.Node})
    background: cc.Node = null;

    @property(cc.Node)
    camera: cc.Node = null;

    @property(cc.Node)
    gameMgr: cc.Node = null;

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    private physicManager: cc.PhysicsManager = null;

    private playerSpeed: number = 0;

    private LDown: boolean = false; // key for player to go left

    private RDown: boolean = false; // key for player to go right

    private spaceDown: boolean = false; // key for player to shoot

    private isJumping: boolean = false;

    private isBig: boolean = false;

    private isDie: boolean = false;

    private offsetX: number = 480;

    private offsetY: number = 320;

    onLoad()
    {
        this.physicManager = cc.director.getPhysicsManager();
        this.physicManager.enabled = true;
        //this.physicManager.gravity = cc.v2 (0, -3000);
        //this.physicManager.gravity = cc.v2 (0, -200);
        
        this.anim = this.getComponent(cc.Animation);

    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);   
    }

    update(dt)
    {   
        if(!this.isDie) {
            if(!this.isBig){
                if(this.LDown && !this.RDown) {
                    if(this.playerSpeed > -300) this.playerSpeed -= 10;
                    if(!this.anim.getAnimationState("mario_small_walk").isPlaying && !this.anim.getAnimationState("mario_jump_up").isPlaying && !this.isJumping) this.animateState = this.anim.play("mario_small_walk");
                }
                    
                if(this.RDown && !this.LDown){
                    if(this.playerSpeed < 300) this.playerSpeed += 10;
                    if(!this.anim.getAnimationState("mario_small_walk").isPlaying && !this.anim.getAnimationState("mario_jump_up").isPlaying  && !this.isJumping) this.animateState = this.anim.play("mario_small_walk");
                }
                    
                if(!this.LDown && !this.RDown){
                    if(this.playerSpeed < 0) this.playerSpeed += 10;
                    else if(this.playerSpeed > 0) this.playerSpeed -= 10;
                    if(!this.isJumping && (this.playerSpeed < 30 || this.playerSpeed > -30) && !this.anim.getAnimationState("mario_idle").isPlaying) this.animateState = this.anim.play("mario_idle");
                }
            }
            else if(this.isBig){
                if(this.LDown && !this.RDown) {
                    if(this.playerSpeed > -300) this.playerSpeed -= 10;
                    if(!this.anim.getAnimationState("mario_big_walk").isPlaying && !this.anim.getAnimationState("mario_big_jump_up").isPlaying) this.animateState = this.anim.play("mario_big_walk");
                }
                    
                if(this.RDown && !this.LDown){
                    if(this.playerSpeed < 300) this.playerSpeed += 10;
                    if(!this.anim.getAnimationState("mario_big_walk").isPlaying && !this.anim.getAnimationState("mario_big_jump_up").isPlaying) this.animateState = this.anim.play("mario_big_walk");
                }
                    
                if(!this.LDown && !this.RDown){
                    if(this.playerSpeed < 0) this.playerSpeed += 10;
                    else if(this.playerSpeed > 0) this.playerSpeed -= 10;
                    if(!this.isJumping && (this.playerSpeed < 30 || this.playerSpeed > -30) && !this.anim.getAnimationState("mario_big_idle").isPlaying) this.animateState = this.anim.play("mario_big_idle");
                }
            }
            
            this.node.x += this.playerSpeed * dt;  //move player (-459, -292)
            if(this.node.x > 0 && this.node.x + this.offsetX < 4920){
                this.camera.x = this.node.x;
            }
            if(this.node.y > 0 && this.node.y + this.offsetY < 1200){
                this.camera.y = this.node.y;
            }
            if(this.node.y < -280){
                this.isDie = true;
                this.anim.stop();
                this.getComponent(cc.Sprite).spriteFrame = this.marioDie;
                this.playerDie();
            }
        }
        
        
        
    }

    
    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.left:

                this.LDown = true;

                this.RDown = false;

                this.node.scaleX = -1;

                //if(this.playerSpeed > -451) this.playerSpeed += -150;

                
                //cc.log("z down");

                break;

            case cc.macro.KEY.right:

                this.RDown = true;

                this.LDown = false;

                this.node.scaleX = 1;

                //if(this.playerSpeed < 451) this.playerSpeed += 150;

                //this.animateState = this.anim.play("mario_small_walk");
                //cc.log("x down");

                break;

            case cc.macro.KEY.space:

                this.spaceDown = true;
                this.node.scaleX = (this.LDown) ? -1 : (this.RDown) ? 1 : this.node.scaleX;

                
                this.playerJump();
                this.isJumping = true;

                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.left:

                this.LDown = false;
                //this.playerSpeed = 0;
                break;

            case cc.macro.KEY.right:

                this.RDown = false;
                //this.playerSpeed = 0;
                break;

            case cc.macro.KEY.space:

                this.spaceDown = false;
                //this.isJumping = false;
                break;
        }
    }

    playerJump()
    {
        //if(!this.fallDown)
        if(this.isJumping) return;
        cc.audioEngine.playEffect(this.jumpSound, false);
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 1200);
        if(!this.isBig && this.animateState != this.anim.getAnimationState("mario_jump_up")) this.animateState = this.anim.play("mario_jump_up");
        else if(this.isBig && this.animateState != this.anim.getAnimationState("mario_big_jump_up")) this.animateState = this.anim.play("mario_big_jump_up");
    }

    playerDie(){
        
        this.getComponent(cc.Sprite).spriteFrame = this.marioDie;
        cc.audioEngine.stopAll();
        cc.director.getActionManager().removeAllActions();
        //cc.director.pause();
        //if(cc.audioEngine.isMusicPlaying()) cc.audioEngine.stopMusic();

        cc.audioEngine.playEffect(this.dieSound, false);
        
        //this.getComponent(cc.Sprite).spriteFrame = this.marioDie;
        let action = cc.sequence(
            cc.moveBy(0.5, 0, 100).easing(cc.easeInOut(2.0)),
            cc.moveTo(2, this.node.x, -800).easing(cc.easeInOut(2.0))
        );
        this.scheduleOnce(() =>{
            this.node.runAction(action);
        }, 0.7);
        /*
        this.scheduleOnce(() =>{
            cc.director.loadScene("howtoplay");
        }, 4);
        */
        this.gameMgr.getComponent("gameMgr").record(this.isDie, this.isBig);
    }

    stageClear(){
        
        this.gameMgr.getComponent("gameMgr").record(this.isDie, this.isBig);
        /*
        cc.audioEngine.stopAll();
        cc.audioEngine.playEffect(this.stageClearSound, false);
        cc.director.pause();
        */
        
    }

    onBeginContact(contact, self, other)
    {
        // mushroom, goomba
        if(other.tag == 100){
            // mushroom destroy
            //other.active = false;
            other.getComponent(cc.RigidBody).enabledContactListener = false;
            other.getComponent(cc.Sprite).enabled = false;
            other.node.destroy();
            //cc.log(other.node.name);

            if(!this.isBig){
                // powerup audioEffect
                cc.audioEngine.playEffect(this.powerup, false);

                // player power up (state?)
                this.isBig = true;
                this.node.width *= 1.05;
                this.node.height *= 1.15;
                this.getComponent(cc.PhysicsBoxCollider).size.width *= 1.05;
                this.getComponent(cc.PhysicsBoxCollider).size.height *= 1.15;

                cc.director.pause();
                setTimeout(()=>{
                    cc.director.resume();
                }, 1500);
            }
            else {
                // player got 500 points
                this.gameMgr.getComponent("gameMgr").playerScoreAdd(500);
                cc.audioEngine.playEffect(this.reserve, false);
            }
            
        }
        else if(other.node.name == "goomba"){
            if(other.getComponent(cc.Sprite).spriteFrame == this.goombaDie) contact.disabled = true;
            else if(contact.getWorldManifold().normal.y >= 0){
                //contact.disabled = true;
                
                if(!this.isBig){
                    other.getComponent(cc.RigidBody).enabledContactListener = false;
                    // player die
                    this.physicManager.enabled = false;
                    //this.getComponent(cc.RigidBody).enabled = false;
                    this.getComponent(cc.PhysicsBoxCollider).enabled = false;
                    this.isDie = true;
                    
                    this.anim.stop();
                    //this.getComponent(cc.Sprite).spriteFrame = this.marioDie;
                    this.playerDie();
                }
                else {
                    // player power down
                    this.isBig = false;
                    this.node.width /= 1.05;
                    this.node.height /= 1.15;
                    this.getComponent(cc.PhysicsBoxCollider).size.width /= 1.05;
                    this.getComponent(cc.PhysicsBoxCollider).size.height /= 1.15;
                    cc.audioEngine.playEffect(this.powerdown, false);

                    this.getComponent(cc.RigidBody).enabledContactListener = false;
                    cc.director.pause();
                    setTimeout(()=>{
                        cc.director.resume();
                        
                        setTimeout(()=>{
                            this.getComponent(cc.RigidBody).enabledContactListener = true;
                        }, 1500);
                        
                    }, 1500);
                }
            }
            else if(contact.getWorldManifold().normal.y < 0){
                cc.audioEngine.playEffect(this.goombaDieSound, false);
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 1200);
                //this.player.getComponent("player").playerJump();

                other.getComponent(cc.RigidBody).enabledContactListener = false;
                other.getComponent(cc.Sprite).spriteFrame = this.goombaDie;
                other.node.stopAllActions();
                    
                //this.getComponent(cc.PhysicsBoxCollider).enabled = false;
                //this.getComponent("player").rigidbody.linearVelocity = cc.v2(0, 300);
                this.scheduleOnce( () =>{
                    other.destroy();
                }, 1);
                
                // player got 100 points
                this.gameMgr.getComponent("gameMgr").playerScoreAdd(100);
            }
        }
        else if(other.node.name == "flower"){
            if(!this.isBig){
                other.getComponent(cc.RigidBody).enabledContactListener = false;
                // player die
                this.physicManager.enabled = false;
                //this.getComponent(cc.RigidBody).enabled = false;
                //this.getComponent(cc.PhysicsBoxCollider).enabled = false;
                this.isDie = true;
                
                this.anim.stop();
                //this.getComponent(cc.Sprite).spriteFrame = this.marioDie;
                this.playerDie();
            }
            else {
                // player power down
                this.isBig = false;
                this.node.width /= 1.05;
                this.node.height /= 1.15;
                this.getComponent(cc.PhysicsBoxCollider).size.width /= 1.05;
                this.getComponent(cc.PhysicsBoxCollider).size.height /= 1.15;
                cc.audioEngine.playEffect(this.powerdown, false);
                contact.disabled = true;
                this.getComponent(cc.RigidBody).enabledContactListener = false;
                cc.director.pause();
                
                setTimeout(()=>{
                    cc.director.resume();
                    setTimeout(()=>{
                        this.getComponent(cc.RigidBody).enabledContactListener = true;
                    }, 1500);
                }, 1500);
            }
            //this.playerDie();
        }
        
        else if(other.node.name == "flag"){
            if(this.node.y >= 500){
                
            }
            this.stageClear();
        }

        else if(contact.getWorldManifold().normal.y < 0){
            if(other.node != null){
                //cc.log("on ground");
                this.isJumping = false;
            }
            //if(other.node.name == "newStage1" && other.getComponent(cc.PhysicsBoxCollider).t)
        }
        
        
        
    }
}