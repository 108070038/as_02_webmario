const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {
    
    @property({type:cc.AudioClip})
    select: cc.AudioClip = null;

    private txtEmail: string = "";

    private txtPassword: string = "";

    onload(){
        
    }

    loginBtn(){

        this.playSound(this.select);

        let par = this.node.getChildByName("text_bg");
        this.txtEmail = par.getChildByName("email_txt").getComponent(cc.EditBox).string;
        this.txtPassword = par.getChildByName("password_txt").getComponent(cc.EditBox).string;
        
        // email, password, life, state, highscore
        firebase.auth().signInWithEmailAndPassword(this.txtEmail, this.txtPassword).then(function(result) {
            cc.director.loadScene("howtoplay");

        }).catch(function(error) {
            alert("Error!!!\n" + error);

        });
    }

    playSound(clip: cc.AudioClip){
        cc.audioEngine.playEffect(clip, false);
    }

    loadMenuScene(){
        this.playSound(this.select);
        cc.director.loadScene("menu");
    }

    
}
