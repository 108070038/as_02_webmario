const {ccclass, property} = cc._decorator;

@ccclass
export default class howtoplay extends cc.Component {
    
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    select: cc.AudioClip = null;

    @property({type:cc.Camera})
    camera: cc.Camera = null;

    @property({type:cc.Node})
    userdata: cc.Node = null;

    @property({type:cc.Node})
    stage: cc.Node = null;

    
    

    start(){
        
        var user = firebase.auth().currentUser;
        var tEmail = user.email;
        var userRef = firebase.database().ref('users/' + tEmail.replace('.', ''));
        
        
        userRef.once('value').then((snapshot) => {
            if (snapshot.exists()) {
              let user_name = snapshot.val().username;
              let life = snapshot.val().life;
              let state = snapshot.val().state;
              let high = snapshot.val().highscore;
              this.userdata.getComponent(cc.Label).string = user_name + '\n\n' + tEmail + '\n\n' + life + '\n\n' + state + '\n\n' + high;
            }
            else {
              alert("No User!!");
            }
        });
        
    }

    moveCamera(event, CustomEventData){
        this.playSound(this.select);
        let offset = 970;
        if(CustomEventData == -1) offset *= -1;
        let action = cc.moveBy(1, offset, 0).easing(cc.easeIn(3.0));
        this.camera.node.runAction(action);
    }
/*
    playBGM(){
        this.audioID = cc.audioEngine.playMusic(this.bgm, true);
    }

    setBgnVol(){
        var vol = this.node.getChildByName("slider").getComponent(cc.Slider).progress;
        cc.audioEngine.setVolume(this.audioID, vol);
    }
*/
    playSound(clip: cc.AudioClip){
        cc.audioEngine.playEffect(clip, false);
    }

    loadGameScene(event, CustomEventData){
        this.playSound(this.select);
        cc.director.loadScene("loading");
        Global.next_stage = CustomEventData;
        Global.now_stage = CustomEventData;
    }

    stageShow(){
        this.playSound(this.select);
        //cc.find("howtoplay_bg/stage_choosing").active = true;
        this.stage.active = true;
    }

    stageHide(){
        this.playSound(this.select);
        this.stage.active = false;
        //cc.find("howtoplay_bg/stage_choosing").active = false;
    }
}
