const { ccclass, property } = cc._decorator;

@ccclass
export default class Others extends cc.Component {

    @property(cc.SpriteFrame)
    goombaDie: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    qbox_n: cc.SpriteFrame = null;

    @property({type:cc.AudioClip})
    qboxPop: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    qbox_mPop: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    goombaDieSound: cc.AudioClip = null;

    @property(cc.Node)
    player: cc.Node = null;

    @property(cc.Node)
    coin: cc.Node = null;

    @property(cc.Node)
    mushroom: cc.Node = null;

    @property(cc.Node)
    camera: cc.Node = null;

    @property({type:cc.Node})
    gameMgr: cc.Node = null;

    @property({type:cc.Node})
    background: cc.Node = null;

    @property({type:cc.Node})
    stage: cc.Node = null;

    protected isTouched: boolean = false;

    private anim: cc.Animation = null;
    
    private animateState: cc.AnimationState = null;

    private moveSpeed: number = 50;

    private mushroom_dir: number = 0;

    private goombaX: number = 0;

    private goombaDir: number = -1;

    //private camera: cc.Node = null;

    start() {
        this.anim = this.getComponent(cc.Animation);

        //this.camera = cc.find('Canvas/Main Camera');

        if (this.node.name == "goomba") {
            //let a = (Math.random() > 0.49) ? -1 : 1;
            this.goombaMove(this.goombaDir, 0);
            this.goombaX = this.node.x;
        }
        else if(this.node.name == "flower"){
            this.node.zIndex = this.stage.zIndex - 1;
            this.background.zIndex = this.node.zIndex - 1;
            this.flowerMove();
        }
        else if(this.node.name == "qbox_m"){
            this.node.zIndex = this.mushroom.zIndex - 1;
        }

    }

    update(dt)
    {   
        if(this.node.name == "goomba" && this.getComponent(cc.Sprite).spriteFrame != this.goombaDie){
            this.node.x += this.goombaDir * 2;
        }
    }

    flowerMove()
    {
        let action = cc.sequence(
            cc.moveBy(1.2, 0, 72),
            cc.moveBy(2, 0, 0),
            cc.moveBy(1.2, 0, -72),
            cc.moveBy(2, 0, 0)
        );
        this.node.runAction(action.repeatForever());
        //this.anim.play("flower_ud");
    }

    goombaMove(moveDir: number, delayTime: number)
    {
        if (this.node.name == "goomba") {
            /*
            let action = cc.spawn(
                cc.scaleBy(0, -1, 1),
                cc.moveBy(0.5, 50 * moveDir, 0)
            );

            this.scheduleOnce(function(){
                this.node.runAction(action).repeatForever();
            }, delayTime);
            */
            let action = cc.spawn(
                cc.scaleBy(0, -1, 1),
                cc.delayTime(0.3)
            );
            this.scheduleOnce(function(){
                this.node.runAction(action).repeatForever();
            }, delayTime);
        }
        
    }

    onBeginContact(contact, self, other) {
       if(this.node.name == "qbox"){
            if(other.node.name == "player" && contact.getWorldManifold().normal.y < 0){
                if(this.getComponent(cc.Sprite).spriteFrame != this.qbox_n){
                    // qbox audioEffect
                    cc.audioEngine.playEffect(this.qboxPop, false);

                    // qbox animation
                    this.node.runAction(cc.jumpBy(0.2, 0, 0, 10, 1));
                    this.anim.stop();
                    this.getComponent(cc.Sprite).spriteFrame = this.qbox_n;

                    // coin animation
                    this.coin.getComponent(cc.Sprite).enabled = true;
                    this.coin.runAction(cc.jumpBy(0.4, 0, 0, 100, 1));
                    this.scheduleOnce( () =>{
                        this.coin.getComponent(cc.Animation).play("coin_die");
                    }, 0.4);
                    
                    // coin destroy
                    this.scheduleOnce( () =>{
                        this.coin.destroy();
                    }, 1);
                    
                    // player got 100 points
                    this.gameMgr.getComponent("gameMgr").playerScoreAdd(100);
               }
               
           }
        }
        else if(this.node.name == "qbox_m"){
            if(other.node.name == "player" && contact.getWorldManifold().normal.y < 0){
                if(this.getComponent(cc.Sprite).spriteFrame != this.qbox_n){
                    // qbox_m audioEffect
                    cc.audioEngine.playEffect(this.qbox_mPop, false);

                    // qbox_m animation
                    this.node.runAction(cc.jumpBy(0.2, 0, 0, 10, 1));
                    this.anim.stop();
                    this.getComponent(cc.Sprite).spriteFrame = this.qbox_n;
                    
                    // mushroom moving direction
                    this.mushroom.getComponent(cc.Sprite).enabled = true;
                    
                    this.mushroom.runAction(cc.moveBy(0.5, 0, 40));
                    /*this.scheduleOnce( () =>{
                        //this.mushroom.getComponent(cc.PhysicsBoxCollider).sensor = false;
                        if(this.mushroom != null){
                            this.mushroom_dir = (Math.random() > 0.49) ? -1 : 1;
                            this.mushroom.getComponent(cc.RigidBody).linearVelocity = cc.v2(140 * this.mushroom_dir, 0);
                        }
                        
                    }, 0.7);
                    */
                }
            }
        }
        else if(this.node.name == "flower" && other.node.name =="newStage1"){
            contact.disabled = true;
        }
        else if(other.tag == 100 && this.node.name == "goomba"){
            contact.disabled = true;
        }
        else if(this.node.name == "goomba" && contact.getWorldManifold().normal.y >= 0 && other.node.name != "player" && this.getComponent(cc.Sprite).spriteFrame != this.goombaDie){
            this.goombaDir *= -1;
            //this.node.x += this.goombaDir * 5;
        }
    }

}
