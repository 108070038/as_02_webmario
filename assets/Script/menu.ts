const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {
    
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    select: cc.AudioClip = null;

    @property({type:cc.Slider})
    slider: cc.Slider = null;

    private audioID: number = 0;

    // ===================== TODO =====================
    // 1. Add dynamic click event to StartButton to call this
    //    function

    start(){
        
        if(!cc.audioEngine.isMusicPlaying()) 
            this.playBGM();
    }

    playBGM(){
        this.audioID = cc.audioEngine.playMusic(this.bgm, true);
    }

    setBgnVol(){
        var vol = this.slider.getComponent(cc.Slider).progress;
        cc.audioEngine.setVolume(this.audioID, vol);
    }

    playSound(clip: cc.AudioClip){
        cc.audioEngine.playEffect(clip, false);
    }

    loadGameScene(){
        cc.director.loadScene("game");
    }

    loadLoginScene(){
        this.playSound(this.select);
        cc.director.loadScene("login");
    }

    loadSignUpScene(){
        this.playSound(this.select);
        cc.director.loadScene("signup");
    }
    // ================================================
}
