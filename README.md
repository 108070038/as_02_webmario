# Software Studio 2021 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|Y|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description 

# Basic Components Description : 
遊戲連結 : https://hw2mario.web.app

1. World map : 
    
    World 1 (Easy) : 玩家起始位置在地圖最左方，終點有上下兩個，上方無怪物但較難行走，下方有怪物。如果要走下方路線先透過上方路線的踏板，否則直接跳是無法安全越過橫溝。
    
    World 2 (Dangerous) : 玩家起始位置在地圖最左方，與World 1相同有兩個路線，但這次只有下方可以通行，上方則是死路。地圖中有多處斜坡，馬力歐在斜坡上會直接滑到最底or直到撞到障礙物。此外，地圖中有多個地方藏有隱形踏板，可以多方嘗試以找到安全通關的路線。
    (hint: 第一朵花的上方、斜坡有玄機)
2. Player : 有大小兩個型態，當吃到蘑菇時會變大。如果再大型態碰到怪物會縮回小型態，再受到傷害便會死亡。注意 : 若是掉洞的話無論大小型態直接死亡。轉換型態時會有短暫停頓。由大轉小時短時間內無敵但無法跳。左右控制移動，空白鍵控制跳。(其他遊戲裡寫的指令來不及做)
3. Enemies : 有兩種怪物，

    goomba : 會在陸地上正常走路(無法爬上斜坡)，碰到障礙物時會改變行進方向。踩在頭上可以殺死。
    
    flower : 一開始隱藏在水管中，經過一段時間後會探出頭來透氣。玩家只要碰到就立即死亡，此怪物不死。
4. Question Blocks : 有兩種 question blocks，第一種是普通型，由下方碰撞後會彈出金幣，每個可得100分。第二種為蘑菇型，會彈出蘑菇，吃了之後若玩家是小型態則會進入大型態，若玩家已經是大型態了則會得到500分。
5. Animations : 玩家部分有走路、跳、死亡等。怪物部分goomba有走路、死亡動畫，flower有上下移動、嘴巴開合的動畫。
6. Sound effects : 

    player : 變大時有power up的音效，縮小時也有power down的音效，另外跳躍、死亡、過關也有。

    怪物 : goomba死亡音效。

    question blocks : 金幣彈出音效、蘑菇彈出音效。

    menu : 按鍵音效。
7. UI : 破關時螢幕上方橫排分別有 world, life, time, score 的資訊。當玩家死亡、得分時都會變動。time會從 300 慢慢減少到 0。

# Bonus Functions Description : 
1. Music Volume : 在 menu 選單頁面左下角可以調整背景音樂音量。

